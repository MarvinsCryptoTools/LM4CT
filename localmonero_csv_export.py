import argparse
import csv
import decimal
import json
import os
from dataclasses import dataclass
from decimal import Decimal
from typing import List
from typing import Optional

import arrow
from agoradesk_py.agoradesk import AgoraDesk
from dateutil.parser import parse
from rich.traceback import install
from sortedcontainers import SortedList
from textual.app import App
from textual.reactive import Reactive
from textual.widgets import Footer
from textual.widgets import Header
from textual.widgets import ScrollView

install(show_locals=True)

__author__ = "marvin8"
__copyright__ = "(C) 2021 https://codeberg.org/MarvinsCryptoTools/LM4CT"
__version__ = "0.1.0"


@dataclass
class Configuration:
    local_monero_api_key: str
    time_zone: str = "UTC"
    create_non_xmr_deposit: bool = False
    create_non_xmr_withdrawals: bool = False
    export_after_timestamp: arrow = arrow.get(0)

    def __init__(self, filename: str = "config.json") -> None:

        self.local_monero_api_key = ""

        if os.path.exists(filename):
            with open(file=filename, encoding="UTF-8") as config_file:
                config_dict = json.load(config_file)
        else:
            config_dict = {}

        self.local_monero_api_key = config_dict.get("local_monero_api_key", "")
        self.time_zone = config_dict.get("time_zone", "UTC")
        self.create_non_xmr_deposit = config_dict.get("create_non_xmr_deposit", False)
        self.create_non_xmr_withdrawals = config_dict.get(
            "create_non_xmr_withdrawals", False
        )
        self.export_after_timestamp = arrow.get(
            config_dict.get("export_after_timestamp", 0)
        )

    def save(self, filename: str = "config.json") -> None:
        config_dict = {
            "local_monero_api_key": self.local_monero_api_key,
            "time_zone": self.time_zone,
            "create_non_xmr_deposit": self.create_non_xmr_deposit,
            "create_non_xmr_withdrawals": self.create_non_xmr_withdrawals,
            "export_after_timestamp": str(self.export_after_timestamp),
        }
        with open(file=filename, mode="w", encoding="UTF-8") as config_file:
            json.dump(config_dict, config_file, indent=4)


@dataclass
class ExportRow:
    # pylint: disable=too-many-instance-attributes
    # All these attributes are needed. Additionally, code is very simple.
    type: str
    purchase_amount: Optional[Decimal] = None
    purchase_currency: Optional[str] = None
    sell_amount: Optional[Decimal] = None
    sell_currency: Optional[str] = None
    fee_amount: Optional[Decimal] = None
    fee_currency: Optional[str] = None
    exchange: Optional[str] = "LocalMonero"
    group: Optional[str] = None
    comment: Optional[str] = None
    transaction_date: arrow = arrow.now()

    def csv_row(self) -> List[str]:
        return [
            self.type,
            str(self.purchase_amount) if self.purchase_amount else "",
            self.purchase_currency if self.purchase_currency else "",
            str(self.sell_amount) if self.sell_amount else "",
            self.sell_currency if self.sell_currency else "",
            str(self.fee_amount) if self.fee_amount else "",
            self.fee_currency if self.fee_currency else "",
            self.exchange if self.exchange else "",
            self.group if self.group else "",
            self.comment if self.comment else "",
            str(self.transaction_date),
        ]

    @staticmethod
    def csv_header() -> List[str]:
        return [
            "Type",
            "Buy",
            "Cur.",
            "Sell",
            "Cur.",
            "Fee",
            "Cur.",
            "Exchange",
            "Group",
            "Comment",
            "Date",
        ]

    def __lt__(self, other):
        return self.transaction_date < other.transaction_date


@dataclass
class DisplayRow:
    transaction_date: arrow
    display: str

    def __lt__(self, other):
        return self.transaction_date < other.transaction_date


def establish_config(args: argparse.Namespace) -> Configuration:
    config = Configuration()
    if args.apikey:
        config.local_monero_api_key = args.apikey
    if args.tz:
        config.time_zone = args.tz
    if args.start_from:
        parsed_date = parse(args.start_from)
        config.export_after_timestamp = arrow.get(parsed_date).replace(
            tzinfo=config.time_zone
        )
    if args.non_xmr_deposits:
        config.create_non_xmr_deposit = args.non_xmr_deposits
    if args.non_xmr_withdrawals:
        config.create_non_xmr_withdrawals = args.non_xmr_withdrawals
    if args.non_xmr_deposits:
        config.create_non_xmr_deposit = args.non_xmr_deposits
    if args.safe:
        config.save()
    return config


def setup_argparser() -> argparse.ArgumentParser:
    arguments_parser = argparse.ArgumentParser(
        description="Export trades and wallet transactions to a CSV file"
    )
    arguments_parser.add_argument(
        "-a",
        "--apikey",
        type=str,
        help="API key from LocalMonero",
        dest="apikey",
    )
    arguments_parser.add_argument(
        "-s",
        "--start-from",
        type=str,
        help="All trades and transactions after this date and time will be "
        "included in the export",
        dest="start_from",
    )
    arguments_parser.add_argument(
        "-z",
        "--time_zone",
        type=str,
        help="Time zone to use for all dates and times used",
        dest="tz",
    )
    arguments_parser.add_argument(
        "--create-non-xmr-deposits",
        action="store_true",
        help="If present, will generated non XMR deposit records "
        "for currencies sold for XMR",
        dest="non_xmr_deposits",
    )
    arguments_parser.add_argument(
        "--create-non-xmr-withdrawals",
        action="store_true",
        help="If present, will generated non XMR withdrawal records "
        "for currencies bought with XMR",
        dest="non_xmr_withdrawals",
    )
    arguments_parser.add_argument(
        "-n",
        "--no-safe",
        action="store_false",
        help="If present, the Configuration will not be saved into the config file."
        " By default the configuration will always be saved after it's been determined",
        dest="safe",
    )
    return arguments_parser


class LocalMoneroExport(App):
    """An example of a very simple Textual App."""

    output = Reactive(False)
    debug_log = Reactive(False)

    def __init__(self, *args, config=Configuration, **kwargs):
        self.config = config
        self.body: ScrollView = None
        self.debug: ScrollView = None
        self.trades = None
        self.transactions = None
        self.who_am_i = None
        super().__init__(*args, **kwargs)

    async def watch_output(self, output: str) -> None:
        """Called when show_bar changes."""
        await self.body.update(output)

    async def watch_debug_log(self, debug_log: str) -> None:
        """Called when show_bar changes."""
        await self.debug.update(debug_log)

    async def on_load(self) -> None:
        """Bind keys with the app loads (but before entering application
        mode)"""
        # await self.bind("b", "view.toggle('sidebar')", "Toggle sidebar")
        await self.bind("d", "run_download", "Download from LocalMonero.co")
        await self.bind("e", "run_export", "Export to csv file")
        await self.bind("l", "view.toggle('debug')", "Toggle debug Log")
        await self.bind("s", "run_safe_log", "Safe Debug Log")
        await self.bind("q", "quit", "Quit")

    async def action_run_download(self) -> None:
        self.output = (
            "Downloading trades and transactions from LocalMonero.co ... "
            "\n\nWe'll be back in a moment"
        )

        api = AgoraDesk(api_key=self.config.local_monero_api_key)

        # Get information about logged in user
        self.who_am_i = api.myself()["response"]["data"]["username"]
        self.debug_log = "Who am I\n"
        self.debug_log += json.dumps(self.who_am_i, indent=2)

        # Get information about all released trades
        api_response = api.dashboard_released()
        self.trades = api_response["response"]["data"]["contact_list"]
        self.debug_log += "\n\nTrades:\n"
        self.debug_log += "\n".join(
            json.dumps(trade, indent=2) for trade in self.trades
        )

        # Get information about XMR wallet transactions
        self.transactions = api.wallet_xmr()["response"]["data"]
        self.debug_log += "\n\nTransactions:\n"
        self.debug_log += json.dumps(self.transactions, indent=2)

        self.output = ""
        display_rows = SortedList()

        await self.load_trades(display_rows)
        await self.load_wallet_txs(display_rows)

        for row in display_rows:
            if row.transaction_date > self.config.export_after_timestamp:
                self.output += f"On {str(row.transaction_date)} - {row.display}\n"

    async def load_wallet_txs(self, display_rows):
        if self.transactions:
            for transaction in self.transactions["received_transactions_30d"]:
                display_rows.add(
                    DisplayRow(
                        transaction_date=arrow.get(transaction["created_at"]).to(
                            self.config.time_zone
                        ),
                        display=(
                            f"Received {transaction['amount']} XMR "
                            f"for {transaction['description']}"
                        ),
                    )
                )
            for transaction in self.transactions["sent_transactions_30d"]:
                display_rows.add(
                    DisplayRow(
                        transaction_date=arrow.get(transaction["created_at"]).to(
                            self.config.time_zone
                        ),
                        display=(
                            f"Sent {transaction['amount']} XMR "
                            f"for {transaction['description']}"
                        ),
                    )
                )

    async def load_trades(self, display_rows):
        if self.trades:
            for trade in self.trades:
                trade_data = trade["data"]
                trade_action = None
                if self.who_am_i == trade_data["buyer"]["username"]:
                    trade_action = "bought"
                elif self.who_am_i == trade_data["seller"]["username"]:
                    trade_action = "sold"
                display_rows.add(
                    DisplayRow(
                        transaction_date=arrow.get(trade_data["released_at"]).to(
                            self.config.time_zone
                        ),
                        display=(
                            f"I have {trade_action} {trade_data['amount_xmr']} XMR "
                            f"with a fee of {trade_data['fee_xmr']} XMR "
                            f"for {trade_data['amount']} {trade_data['currency']} "
                            f"({trade_data['contact_id']})"
                        ),
                    )
                )

    async def action_run_export(self) -> None:

        if self.trades is None or self.trades is False:
            self.output = "Download data from LocalMonero.co first"
            return

        self.output = "Export Running ...\n\n"

        context = decimal.getcontext()
        context.prec = 12

        rows = SortedList()

        await self.generate_trade_rows(rows)
        await self.generate_wallet_tx_rows(rows)

        with open(
            "LocalMoneroExport.csv",
            mode="w",
            encoding="utf8",
        ) as export_file:
            csv_writer = csv.writer(export_file, quoting=csv.QUOTE_ALL)
            csv_writer.writerow(ExportRow.csv_header())

            for export_row in rows:
                if export_row.transaction_date > self.config.export_after_timestamp:
                    csv_writer.writerow(export_row.csv_row())

        with open(
            "LocalMoneroExport.csv",
            encoding="utf8",
        ) as csv_file:
            self.output += csv_file.read()

        self.output += "\n\nCSV File saved to 'LocalMoneroExport.csv'"

    async def action_run_safe_log(self) -> None:

        if self.debug_log:
            with open("Debug.log", mode="w", encoding="utf-8") as debug_log_file:
                debug_log_file.write(self.debug_log)

    async def generate_wallet_tx_rows(self, rows):
        if self.transactions:
            for transaction in self.transactions["received_transactions_30d"]:
                if "from an external address" in transaction["description"]:
                    current_row = ExportRow("Deposit")
                    current_row.purchase_amount = Decimal(transaction["amount"])
                    current_row.purchase_currency = "XMR"
                    current_row.transaction_date = arrow.get(
                        transaction["created_at"]
                    ).to(self.config.time_zone)
                    rows.add(current_row)

            for transaction in self.transactions["sent_transactions_30d"]:
                if "Send to" in transaction["description"]:
                    current_row = ExportRow(type="Withdrawal")
                    current_row.sell_amount = Decimal(transaction["amount"])
                    current_row.sell_currency = "XMR"
                    current_row.transaction_date = arrow.get(
                        transaction["created_at"]
                    ).to(self.config.time_zone)
                    rows.add(current_row)

                if "fee" in transaction["description"]:
                    if current_row.sell_amount:
                        current_row.sell_amount += Decimal(transaction["amount"])
                    else:
                        current_row.sell_amount = Decimal(transaction["amount"])
                    current_row.fee_amount = Decimal(transaction["amount"])
                    current_row.fee_currency = "XMR"

    async def generate_trade_rows(self, rows):
        if self.trades:
            for trade in self.trades:
                trade_data = trade["data"]

                current_row = ExportRow(type="Trade")
                current_row.fee_amount = Decimal(trade_data["fee_xmr"])
                current_row.fee_currency = "XMR"
                current_row.comment = trade_data["contact_id"]
                current_row.transaction_date = arrow.get(trade_data["released_at"]).to(
                    self.config.time_zone
                )

                if self.who_am_i == trade_data["buyer"]["username"]:
                    current_row.purchase_amount = Decimal(trade_data["amount_xmr"])
                    current_row.purchase_currency = "XMR"
                    current_row.sell_amount = Decimal(trade_data["amount"])
                    current_row.sell_currency = trade_data["currency"]

                    if self.config.create_non_xmr_deposit:
                        generated_row = ExportRow(type="Deposit")
                        generated_row.purchase_amount = current_row.sell_amount
                        generated_row.purchase_currency = current_row.sell_currency
                        generated_row.transaction_date = (
                            current_row.transaction_date.shift(seconds=-1)
                        )
                        rows.add(generated_row)

                elif self.who_am_i == trade_data["seller"]["username"]:
                    current_row.purchase_amount = Decimal(trade_data["amount"])
                    current_row.purchase_currency = trade_data["currency"]
                    current_row.sell_amount = Decimal(trade_data["amount_xmr"])
                    current_row.sell_currency = "XMR"

                    if self.config.create_non_xmr_withdrawals:
                        generated_row = ExportRow(type="Withdrawal")
                        generated_row.sell_amount = current_row.purchase_amount
                        generated_row.sell_currency = current_row.purchase_currency
                        generated_row.transaction_date = (
                            current_row.transaction_date.shift(seconds=+1)
                        )
                        rows.add(generated_row)

                rows.add(current_row)

    async def on_mount(self) -> None:
        """Create and dock the widgets."""

        self.body = ScrollView(gutter=1)
        self.debug = ScrollView(gutter=1)
        self.debug.visible = False

        # Header / footer / dock
        await self.view.dock(Header(), edge="top")
        await self.view.dock(Footer(), edge="bottom")
        # await self.view.dock(Placeholder(), edge="left", size=30, name="sidebar")
        await self.view.dock(self.debug, edge="left", size=180, name="debug")

        # Dock the body in the remaining space
        await self.view.dock(self.body, edge="right")


def main():
    arguments_parser = setup_argparser()
    args = arguments_parser.parse_args()

    config = establish_config(args)

    LocalMoneroExport.run(title="LocalMonero Export", log="textual.log", config=config)


if __name__ == "__main__":
    main()
