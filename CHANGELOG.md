# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Fixed
- When 'create-non-xmr-withdrawals' is set to true to actually create Withdrawal records in csv file.

### Added
- Debug Log window that can be toggled on using 'L'

## [0.1.0] - *No Releases Yet*
