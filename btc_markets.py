import base64
import datetime
import hashlib
import hmac
import json
import time
from urllib.error import URLError
from urllib.request import Request
from urllib.request import urlopen

base_url = "https://api.btcmarkets.net"


def makeHttpCall(method, apiKey, privateKey, path, queryString, data=None):
    if data is not None:
        data = json.dumps(data)

    headers = buildHeaders(method, apiKey, privateKey, path, data)
    fullPath = ""
    if queryString is None:
        fullPath = path
    else:
        fullPath = path + "?" + queryString

    try:
        http_request = Request(base_url + fullPath, data, headers, method=method)

        if method == "POST" or method == "PUT":
            response = urlopen(http_request, data=bytes(data, encoding="utf-8"))
        else:
            response = urlopen(http_request)

        return json.loads(str(response.read(), "utf-8"))
    except URLError as e:
        errObject = json.loads(e.read())
        if hasattr(e, "code"):
            errObject["statusCode"] = e.code

        return errObject


def buildHeaders(method, apiKey, privateKey, path, data):
    now = str(int(time.time() * 1000))
    message = method + path + now
    if data is not None:
        message += data

    signature = signMessage(privateKey, message)

    headers = {
        "Accept": "application/json",
        "Accept-Charset": "UTF-8",
        "Content-Type": "application/json",
        "BM-AUTH-APIKEY": apiKey,
        "BM-AUTH-TIMESTAMP": now,
        "BM-AUTH-SIGNATURE": signature,
    }

    return headers


def signMessage(privateKey, message):
    presignature = base64.b64encode(
        hmac.new(privateKey, message.encode("utf-8"), digestmod=hashlib.sha512).digest()
    )
    signature = presignature.decode("utf8")

    return signature


class BTCMarkets:
    def __init__(self, apiKey, privateKey):
        self.apiKey = apiKey
        self.privateKey = base64.b64decode(privateKey)

    def get_orders(self):

        return makeHttpCall(
            "GET", self.apiKey, self.privateKey, "/v3/orders", "status=all"
        )

    def place_new_order(self):

        model = {
            "marketId": "BTC-AUD",
            "price": "100.12",
            "amount": "1.034",
            "type": "Limit",
            "side": "Bid",
        }

        return makeHttpCall(
            "POST", self.apiKey, self.privateKey, "/v3/orders", None, model
        )

    def cancel_order(self):

        return makeHttpCall(
            "DELETE", self.apiKey, self.privateKey, "/v3/orders", "id=1224905"
        )

    def get_server_time(self):
        http_request = Request(base_url + "/v3/time", method="GET")
        response = urlopen(http_request)
        return json.loads(str(response.read(), "utf-8"))


api_key = "ec8dc863-e5ad-42f6-9b31-8dc35db23361"
private_key = "zefVeZ1PzE/zGR0HBsKe7tyXiMmip4ZHMs2/cCul4mQhecyVF7B6J3y0A5AMjt9QijTR1le4sleY88fPsBJo+w=="

client = BTCMarkets(api_key, private_key)
timestamp = time.time()
localtime = datetime.datetime.fromtimestamp(timestamp)
utctime = datetime.datetime.fromtimestamp(timestamp, datetime.timezone.utc)
server_time = client.get_server_time()
print(f"{server_time=}")
print(f"{timestamp=}")
print(f"{localtime=}")
print(f"{utctime=}")
print(f"{datetime.datetime.utcnow()=}")
# orders = client.get_orders()
# print(orders)
