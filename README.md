# localmonero_csv_export
Python script to export trades and wallet transaction on LocalMonero.co / AgoraDesk.com to a csv file.
This csv file should be suitable for importing into CoinTracking.info

I am not associated with AgoraDesk.com or LocalMonero.co -- use this at your own risk!

# Requirements:
* httpx
* arrow

# How to Use

Todo: Add instructions

# Contribute
Do you have an idea or found a bug? Please feel free file an issue and/or submit a  PR! :)

## Support
If you like the API and want to support me you can do so with
Monero:
    86twFgKhxQ1DVYyAQLtJZhZQ9vtYxr31Qg5JoVHY2c2YDfr3cnRtE2rcEx1DvzMJ44V7x1JSvzvmHLQzC8p1oM6hDYzzbAC
